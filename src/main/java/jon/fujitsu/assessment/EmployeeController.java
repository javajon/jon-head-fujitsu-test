package jon.fujitsu.assessment;

import jon.fujitsu.assessment.client.RestClient;
import jon.fujitsu.assessment.model.Employee;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * EmployeeController - contains a main method - the starting point used to execute the challenge.
 *
 */
public class EmployeeController {

    final static Logger logger = Logger.getLogger(EmployeeController.class);
    public static void main(String[] args) {
        RestClient client = new RestClient();

        logger.info(String.format("Step 1 of challenge: creating a new Employee"));
        createEmployeeByPost(client, createTestEmployee());

        Employee[] employeesArray = client.getEmployees();
        if (employeesArray == null || employeesArray.length == 0){
            logger.info("No employees were found!");
        } else {
            logger.info("All employees retrieved!");
            logger.info(String.format("Step a of challenge: Total number of employees: %s", employeesArray.length));
        }

        List<Employee> employees = Arrays.asList(employeesArray);

        List<Employee> highSalaryEmployees = employees.stream().filter(employee -> Integer.parseInt(employee.getSalary()) > 500).collect(Collectors.toList());
        logger.info("Step b of challenge: Employees with a salary of over 5000: -");

        for(Employee highSalaryEmployee: highSalaryEmployees){
            logger.info(highSalaryEmployee.getName());
        }

        Comparator<Employee> compById = (employeeA, employeeB) -> Integer.parseInt(employeeB.getId()) - Integer.parseInt(employeeA.getId());
        Employee highestIdEmployee = employees.stream().sorted(compById).findFirst().get();
        logger.info(String.format("Step c of challenge: The Employee with the highest id is %s with an id of %s", highestIdEmployee.getName(), highestIdEmployee.getId()));

        Comparator<Employee> compBySalary = (employeeA, employeeB) -> Integer.parseInt(employeeB.getSalary()) - Integer.parseInt(employeeA.getSalary());
        Employee highestSalaryEmployee = employees.stream().sorted(compBySalary).findFirst().get();
        logger.info(String.format("The Employee with the highest salary is %s with an salary of %s", highestSalaryEmployee.getName(), highestSalaryEmployee.getSalary()));

        Integer highestCurrentSalary = Integer.parseInt(highestSalaryEmployee.getSalary());
        Integer newHighestSalary = highestCurrentSalary + 1;


        logger.info(String.format("Step d, creating an Employee with an salary higher than the current maximum", highestSalaryEmployee.getName(), highestSalaryEmployee.getSalary()));
        Employee newHighestPaidEmployee = createTestEmployee();
        newHighestPaidEmployee.setName("Bill Gates");
        newHighestPaidEmployee.setSalary(newHighestSalary.toString());
        createEmployeeByPost(client, newHighestPaidEmployee);
    }

    private static void createEmployeeByPost(RestClient client, Employee employee) {
        Response response = client.createEmployee(employee);
        if(response.getStatus() == Response.Status.OK.getStatusCode()){
            logger.info("Employee created successfully!");
        } else {
            logger.info(String.format("Problem creating employee. Status code: %s. Employee details: %s", response.getStatus(), employee));
        }
    }

    private static Employee createTestEmployee(){
        Employee employee = new Employee();
        employee.setId("1");
        employee.setName("test");
        employee.setSalary("123");
        employee.setAge("23");
        return employee;
    }
}
