package jon.fujitsu.assessment.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class with which to deserialize Employee objects to a Java POJO from json.
 *
 * Example json source:
 *
 * {
 *   "id": "62232",
 *   "employee_name": "HolaA",
 *   "employee_salary": "123",
 *   "employee_age": "23",
 *   "profile_image": ""
 * }
 *
 */
public class Employee {

    @JsonProperty("id")
    String id;

    @JsonProperty("employee_name")
    String name;

    @JsonProperty("employee_salary")
    String salary;

    @JsonProperty("employee_age")
    String age;

    @JsonProperty("profile_image")
    String profileImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * Compare each property in turn. Only return true if every property between both comparators is equal.
     * @param o - the other instance to compare to this.
     * @return boolean to indicate whether the two objects are truly equal.
     */
    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Employee))
            return false;

        Employee other = (Employee) o;
        if(this.getId()!=null) {
            if (other.getId() == null || this.getId() != other.getId()) {
                return false;
            }
        } else if (other.getId() != null) {
            return false;
        }

        if(this.getName()!=null) {
            if (other.getName() == null || this.getName() != other.getName()) {
                return false;
            }
        } else if (other.getName() != null) {
            return false;
        }

        if(this.getSalary()!=null) {
            if (other.getSalary() == null || !(this.getSalary().equals(other.getSalary()))) {
                return false;
            }
        } else if (other.getSalary() != null) {
            return false;
        }

        if(this.getAge()!=null) {
            if (other.getAge() == null || !(this.getAge().equals(other.getAge()))) {
                return false;
            }
        } else if (other.getAge() != null) {
            return false;
        }

        if(this.getProfileImage()!=null) {
            if (other.getProfileImage() == null || !(this.getProfileImage().equals(other.getProfileImage()))) {
                return false;
            }
        } else if (other.getProfileImage() != null) {
            return false;
        }
        return true;
    }

    /**
     * Display name/value pairs of all properties.
     * @return String
     */
    @Override
    public String toString() {
        return "Employee{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", salary='" + salary + '\'' +
                ", age='" + age + '\'' +
                ", profileImage='" + profileImage + '\'' +
                '}';
    }
}
