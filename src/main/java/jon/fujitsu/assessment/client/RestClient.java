package jon.fujitsu.assessment.client;

import jon.fujitsu.assessment.model.Employee;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Two Rest Client implementations are used here: javax.ws.rs.client.Client and org.springframework.web.client.RestTemplate.
 * This is because, in the limited time used to perfrom the test neither one could be made to successfully perform get
 * and post! So the pragmatic decision was made to use a different client for each of these.
 */
public class RestClient {


    private static final String GET_EMPLOYEES_REST_URL
            = "http://dummy.restapiexample.com/api/v1/employees";
    private static final String POST_EMPLOYEE_REST_URL
            = "http://dummy.restapiexample.com/api/v1/create";

    public Employee[] getEmployees() {
        RestTemplate restTemplate = getRestTemplate();
        Employee[] employees = restTemplate.getForObject(GET_EMPLOYEES_REST_URL, Employee[].class);
        return employees;
    }

    public Employee getEmployee(String url) {
        RestTemplate restTemplate = getRestTemplate();
        Employee employee = restTemplate.getForObject(url, Employee.class);
        return employee;
    }

    private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        //Add the Jackson Message converter
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

        // Note: here we are making this converter to process any kind of response,
        // not only application/*json, which is the default behaviour
        converter.setSupportedMediaTypes(Collections.singletonList(org.springframework.http.MediaType.ALL));
        messageConverters.add(converter);
        restTemplate.setMessageConverters(messageConverters);
        return restTemplate;
    }

    public Response createEmployee(Employee employee) {
        Client client = ClientBuilder.newClient();
        return client
                .target(POST_EMPLOYEE_REST_URL)
                .request(javax.ws.rs.core.MediaType.APPLICATION_JSON)
                .post(Entity.entity(employee, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }
}
