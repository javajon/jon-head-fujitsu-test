package jon.fujitsu.assessment.model;

import org.junit.Test;

public class EmployeeTest {

    @Test
    public void validEquals() {

        Employee employee1 = new Employee();
        employee1.setId("62231");
        employee1.setName("test");
        employee1.setSalary("123");
        employee1.setAge("23");

        Employee employee2 = new Employee();
        employee2.setId("62231");
        employee2.setName("test");
        employee2.setSalary("123");
        employee2.setAge("23");

        assert(employee1.equals(employee2));
    }

    @Test
    public void validNonEquals() {

        Employee employee1 = new Employee();
        employee1.setId("62232");
        employee1.setName("test");
        employee1.setSalary("123");
        employee1.setAge("23");

        Employee employee2 = new Employee();
        employee2.setId("62231");
        employee2.setName("test");
        employee2.setSalary("123");
        employee2.setAge("23");

        assert(!employee1.equals(employee2));
    }

    @Test
    public void validToString() {

        Employee employee1 = new Employee();
        employee1.setId("62232");
        employee1.setName("test");
        employee1.setSalary("123");
        employee1.setAge("23");
        employee1.setProfileImage("Mona Lisa");

        String toStringResult = employee1.toString();
        assert(toStringResult.contains(employee1.getAge()));
        assert(toStringResult.contains(employee1.getId()));
        assert(toStringResult.contains(employee1.getName()));
        assert(toStringResult.contains(employee1.getProfileImage()));
        assert(toStringResult.contains(employee1.getSalary()));
    }

}
