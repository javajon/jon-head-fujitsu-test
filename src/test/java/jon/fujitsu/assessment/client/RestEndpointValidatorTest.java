package jon.fujitsu.assessment.client;

import io.restassured.http.ContentType;
import org.junit.Test;

import static io.restassured.RestAssured.given;

/**
 * Simple Rest Assured QA to ensure the endpoints are up and responsive.
 */
public class RestEndpointValidatorTest {

    private static final String GET_EMPLOYEE_REST_URL
            = "http://dummy.restapiexample.com/api/v1/employee";
    private static final String GET_EMPLOYEES_REST_URL
            = "http://dummy.restapiexample.com/api/v1/employees";
    private static final String POST_EMPLOYEE_REST_URL
            = "http://dummy.restapiexample.com/api/v1/create";

    @Test
    public void test_GetEmployeesEndpoint_ValidResponse() {

        given().
                when().
                get(GET_EMPLOYEES_REST_URL).
                then().
                assertThat().
                contentType(ContentType.HTML).
                and().
                statusCode(200);
    }

    @Test
    public void test_PostEmployeeEndpoint_ValidResponse() {

        given().
                when().
                post(POST_EMPLOYEE_REST_URL).
                then().
                assertThat().
                contentType(ContentType.HTML).
                and().
                statusCode(200);
    }

}
