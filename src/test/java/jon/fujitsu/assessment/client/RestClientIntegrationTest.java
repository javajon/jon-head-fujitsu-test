package jon.fujitsu.assessment.client;


import jon.fujitsu.assessment.model.Employee;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

//Ignoring class until we can sort out the json bind issues
@Ignore
public class RestClientIntegrationTest {

    private static final String GET_EMPLOYEE_REST_URL
            = "http://dummy.restapiexample.com/api/v1/employee/62231";
    private jon.fujitsu.assessment.client.RestClient client = new RestClient();

    @Test
    public void getValidUser() {

        Employee expectedEmployee = new Employee();
        expectedEmployee.setId("62231");
        expectedEmployee.setName("test");
        expectedEmployee.setSalary("123");
        expectedEmployee.setAge("23");
        RestClient client = new RestClient();
        Employee actualEmployee = client.getEmployee(GET_EMPLOYEE_REST_URL);
        assert(actualEmployee != null);
        assert(expectedEmployee.equals(actualEmployee));
    }

    @Test
    public void getValidUserIncorrectMatch() {
        Employee expectedEmployee = new Employee();
        expectedEmployee.setId("62231");
        expectedEmployee.setName("test");
        expectedEmployee.setSalary("123");
        expectedEmployee.setAge("23");

        RestClient client = new RestClient();

        Employee actualEmployee = client.getEmployee(GET_EMPLOYEE_REST_URL);
        assert(actualEmployee != null);
        assert(!expectedEmployee.equals(actualEmployee));
    }

    @Test
    public void getAllEmployees() {
        RestClient client = new RestClient();

        Employee[] employeesArray = client.getEmployees();
        assert(employeesArray != null);

        List<Employee> employees = Arrays.asList(employeesArray);


    }
}